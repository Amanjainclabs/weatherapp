import UIKit

class SecondViewController: UIViewController {

    // image view to load desirable image
    @IBOutlet weak var imageViewer: UIImageView!
    
    // various outlets of labels defined
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var dayName: UILabel!
    @IBOutlet weak var monthName: UILabel!
    @IBOutlet weak var dateName: UILabel!
    @IBOutlet weak var timeDisplay: UILabel!
    @IBOutlet weak var meridiemDisplay: UILabel!
    @IBOutlet weak var forecastDisplay: UILabel!
    @IBOutlet weak var zoneDisplay: UILabel!
    @IBOutlet weak var maximumTemperature: UILabel!
    @IBOutlet weak var minimumTemperature: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // assigning global values as contents of various labels
        cityName.text = MyCity
        dayName.text = Day
        monthName.text = Month
        dateName.text = Date
        timeDisplay.text = Time
        meridiemDisplay.text = Meridiem
        forecastDisplay.text = DetailedForecast
        zoneDisplay.text = TimeZone
        maximumTemperature.text = Maximum
        minimumTemperature.text = Minimum
        imageDisplay()
    }
    
    // display images in image view according to the time
    func imageDisplay() {
        
        var checkTime = (Time.componentsSeparatedByString(":")[0]).toInt()
        if Meridiem == "AM" {
            if checkTime < 5 {
                imageViewer.image = UIImage(named: "Night.jpg")
            }else if checkTime < 7 {                
                imageViewer.image = UIImage(named: "Sunrise.jpg")
            }else {
                imageViewer.image = UIImage(named: "Noon.jpg")
            }
        }else {
            if checkTime < 4 {
                imageViewer.image = UIImage(named: "Noon.jpg")
            }else if checkTime < 8 {
                imageViewer.image = UIImage(named: "Evening.jpg")
            }else {
                imageViewer.image = UIImage(named: "Night.jpg")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // force stop auto rotation and force portrait orientation 
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Portrait.rawValue)
    }
}