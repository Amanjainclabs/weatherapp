import UIKit

// global variables to store date time weather details and maximum and minimum temperature to be used by other view
var Time = String()
var Meridiem = String()
var TimeZone = String()
var Day = String()
var Month = String()
var Date = String()
var Maximum = String()
var Minimum = String()
var MyCity = String()
var DetailedForecast = String()

class ViewController: UIViewController, UITextFieldDelegate{
    
    // variables to store the final weather forecast
    var weatherForecast = String()
    var urlContent = NSString()
    var fewDetails = String()
    
    // outlet takes control to second view control
    @IBOutlet weak var fullDetailButton: UIButton!
    // store the string given in the text field
    @IBOutlet weak var cityName: UITextField!
    // displays how is the weather ignoring the details
    @IBOutlet var weatherDisplay: UILabel!
    @IBOutlet weak var errorMessage: UILabel!
    // instructions run when find button is pressed
    
    @IBAction func getWeather(sender: AnyObject) {
        
        self.errorMessage.text = ""
        // capture only the city name entered in the text field
        MyCity = cityName.text.componentsSeparatedByString(" ")[0]
        self.view.endEditing(true)
        
        // creating the url which valid for finding the forecast
        var urlString = "http://www.weather-forecast.com/locations/" + MyCity.stringByReplacingOccurrencesOfString(" ", withString: "") + "/forecasts/latest"
        
        // converting string to url form
        var url = NSURL(string: urlString)
        
        // creating a task to gather information from the url
        let task = NSURLSession.sharedSession().dataTaskWithURL(url!) {
            (data, response, error ) in
            
            // encoding the website
            self.urlContent = NSString(data: data, encoding: NSUTF8StringEncoding)!
            // checking for a particular clause to determine whether url is correct or not
            var checkPhrase = self.urlContent.containsString("<span class=\"phrase\">")
            if  checkPhrase != false {
                
                /*
                    Below this we will be extracting data from the HTML source page of the website
                    Useful information lies between specific piece of code.
                    We will extract that and discard remaining HTML code
                */
                
                // form an array of strings from a string saperated by occurence of some substring
                var contentArray = self.urlContent.componentsSeparatedByString("<span class=\"phrase\">") as [NSString]
                var newContentArray = contentArray[1].componentsSeparatedByString("</span>")
                
                // replacing string with other string
                self.weatherForecast = newContentArray[0].stringByReplacingOccurrencesOfString("&deg;", withString: "°")
                DetailedForecast = self.weatherForecast
                
                // extracting only the weather general info  
                var clipInfo = self.weatherForecast.stringByReplacingOccurrencesOfString("(", withString: " ")
                var clipFurther = clipInfo.stringByReplacingOccurrencesOfString(",", withString: " ")
                self.fewDetails = clipFurther.componentsSeparatedByString("  ")[0]
                
                // force execution of code in block stopping the ongoing execution
                dispatch_async(dispatch_get_main_queue()){
                        self.getTimeDate()
                        self.maxMinDisplay()
                }
            }else {
                dispatch_async(dispatch_get_main_queue()){
                    // display error message if city name is incorrect
                    let error = " Enter correct city name !!"
                    if  error != "" { self.errorMessage.text! = error }
                }
            }
        }
        // start the task
        task.resume()
    }
    
    func getTimeDate() {
        
        // search string
        var searchTimeString = "Time+in+" + cityName.text.stringByReplacingOccurrencesOfString(" ", withString: "+")
        
        // creating url to be understood by the website
        var dateUrlString = "https://search.yahoo.com/search;_ylt=AwrSbgconMBUH3QAMBlXNyoA;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3NmcARncHJpZANXbWVRZ2lsc1QzU3B3WlQ1WkM5WVJBBG5fcnNsdAMwBG5fc3VnZwMxMARvcmlnaW4Dc2VhcmNoLnlhaG9vLmNvbQRwb3MDMARwcXN0cgMEcHFzdHJsAwRxc3RybAMyMQRxdWVyeQN0aW1lIGluIG1vc2NvdyBydXNzaWEEdF9zdG1wAzE0MjE5MDkwMzg-;_ylc=X1MDMjc2NjY3OQRfcgMyBGZyA3NmcARncHJpZANXbWVRZ2lsc1QzU3B3WlQ1WkM5WVJBBG5fcnNsdAMwBG5fc3VnZwMxMARvcmlnaW4Dc2VhcmNoLnlhaG9vLmNvbQRwb3MDMARwcXN0cgMEcHFzdHJsAwRxc3RybAMyMQRxdWVyeQN0aW1lIGluIG1vc2NvdyBydXNzaWEEdF9zdG1wAzE0MjE5MDkwNDI-?p=" + searchTimeString + "&fr2=sb-top-search&fr=sfp"
        
        // converting string to url
        var dateUrl = NSURL(string: dateUrlString)
        let task = NSURLSession.sharedSession().dataTaskWithURL(dateUrl!) {
            (data, response, error ) in
            var dateUrlContent = NSString(data: data, encoding: NSUTF8StringEncoding)!
            var checkPhrase = dateUrlContent.containsString("<span class=\" fz-xxxl fc-first fw-s\">")
            if  checkPhrase != false {
                
                var contentArray = dateUrlContent.componentsSeparatedByString("<span class=\" fz-xxxl fc-first fw-s\">") as [NSString]
                var newContentArray = contentArray[1].componentsSeparatedByString("</span>")
                var generateDateTime = newContentArray[0] as String
                var dateArray = generateDateTime.componentsSeparatedByString(" ")
                
                dispatch_async(dispatch_get_main_queue()){
                    // extracting various parameters from dataArray string
                    Time = dateArray[0]
                    Meridiem = dateArray[1]
                    TimeZone = dateArray[2]
                    Day = dropLast(dateArray[4])
                    Month = dateArray[5]
                    Date = dateArray[6]
                    // displaying weather and unhiding the button
                    self.weatherDisplay.text! = self.fewDetails
                    self.fullDetailButton.hidden = false
                }
            }else {
                dispatch_async(dispatch_get_main_queue()){
                self.errorMessage.text = "Many cities with that name... \nMention the country name with it. "
                }
            }
        }
        task.resume()
    }
    
    func maxMinDisplay() {
        // gathering information about the temperature from the source code
        Maximum = (weatherForecast.componentsSeparatedByString("max ")[1]).componentsSeparatedByString(" on")[0]
        Minimum = (weatherForecast.componentsSeparatedByString("min ")[1]).componentsSeparatedByString(" on")[0]
    }
    
    // hide the keyboard when touched anywhere on screen
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // hiding the button
        fullDetailButton.hidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // forcing no auto-rotation
    override func shouldAutorotate() -> Bool {
        return false
    }
    
    // forcing portrait view
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Portrait.rawValue)
    }
}